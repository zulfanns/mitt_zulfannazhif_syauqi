﻿using System;
using System.Collections.Generic;

namespace UserSkillsMIH.Models
{
    public partial class Skill
    {
        public Skill()
        {
            UserSkills = new HashSet<UserSkills>();
        }

        public int SkillId { get; set; }
        public string SkillName { get; set; }

        public virtual ICollection<UserSkills> UserSkills { get; set; }
    }
}
