﻿using System;
using System.Collections.Generic;

namespace UserSkillsMIH.Models
{
    public partial class User
    {
        public User()
        {
            UserSkills = new HashSet<UserSkills>();
        }

        public string Username { get; set; }
        public string Password { get; set; }

        public virtual ICollection<UserSkills> UserSkills { get; set; }
    }
}
