﻿using System;
using System.Collections.Generic;

namespace UserSkillsMIH.Models
{
    public partial class SkillLevel
    {
        public SkillLevel()
        {
            UserSkills = new HashSet<UserSkills>();
        }

        public int SkillLevelId { get; set; }
        public string SkillLevelName { get; set; }

        public virtual ICollection<UserSkills> UserSkills { get; set; }
    }
}
